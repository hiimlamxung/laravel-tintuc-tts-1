<?php

use App\Http\Controllers\PagesController;
use Illuminate\Support\Facades\Route;
use App\User;
use App\Models\RoleModel;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Login
Route::group(['prefix' => 'admin', 'namespace' => 'backend','middleware' => 'AdminLoggedMiddleware'], function(){
	Route::get('login', 'UserController@LoginAdmin')->name('login.admin');

	Route::post('postlogin', 'UserController@PostLoginAdmin');
});

//Logout
Route::get('admin/logout','backend\UserController@LogoutAdmin')->name('logout.admin');

//Route các tính năng chính
Route::group(['prefix' => 'admin', 'namespace' => 'backend','middleware' => 'AdminLoginMiddleware'], function(){
	Route::group(['prefix' => 'theloai'], function(){
		//danh sách thể loại
		Route::get('index', 'TheLoaiController@index')->name('theloai.index')->middleware('can:theloai-list');

		//Thêm thể loại
		Route::get('create', 'TheLoaiController@create')->name('theloai.create')->middleware('can:theloai-create');
		Route::post('store', 'TheLoaiController@store')->name('theloai.store')->middleware('can:theloai-create');

		//Sửa
		Route::get('edit/{id}', 'TheLoaiController@edit')->name('theloai.edit')->middleware('can:theloai-edit');
		Route::post('update/{id}', 'TheLoaiController@update')->name('theloai.update')->middleware('can:theloai-edit');

		//Xóa
		Route::get('delete/{id}', 'TheLoaiController@destroy')->name('theloai.delete')->middleware('can:theloai-delete');
	});

	Route::group(['prefix' => 'loaitin'], function(){
		//danh sách loại tin
		Route::get('index', 'LoaiTinController@index')->middleware('can:loaitin-list');

		//Thêm loại tin
		Route::get('create', 'LoaiTinController@create')->name('loaitin.create')->middleware('can:loaitin-create');
		Route::post('store', 'LoaiTinController@store')->name('loaitin.store')->middleware('can:loaitin-create');

		//Sửa
		Route::get('edit/{id}', 'LoaiTinController@edit')->name('loaitin.edit')->middleware('can:loaitin-edit');
		Route::post('update/{id}', 'LoaiTinController@update')->name('loaitin.update')->middleware('can:loaitin-edit');

		//Xóa
		Route::get('delete/{id}', 'LoaiTinController@destroy')->name('loaitin.delete')->middleware('can:loaitin-delete');
	});

	Route::group(['prefix' => 'tintuc'], function(){
		Route::get('index', 'TinTucController@index')->name('tintuc.index')->middleware('can:tintuc-list');

		Route::get('create', 'TinTucController@create')->name('tintuc.create')->middleware('can:tintuc-create');
		Route::post('store', 'TinTucController@store')->name('tintuc.store')->middleware('can:tintuc-create');

		Route::get('edit/{id}', 'TinTucController@edit')->name('tintuc.edit')->middleware('can:tintuc-edit');
		Route::post('update/{id}', 'TinTucController@update')->name('tintuc.update')->middleware('can:tintuc-edit');

		Route::get('delete/{id}', 'TinTucController@destroy')->name('tintuc.delete')->middleware('can:tintuc-delete');
	});

	//Ajax
	Route::group(['prefix' => 'ajax'], function(){
		Route::get('loaitin/{idTheLoai}', 'AjaxController@getLoaiTin');
	});


	Route::group(['prefix' => 'user'], function(){
		Route::get('index', 'UserController@index')->middleware('can:user-list');

		Route::get('create', 'UserController@create')->name('user.create')->middleware('can:user-create');
		Route::post('store','UserController@store')->name('user.store')->middleware('can:user-create');

		Route::get('edit/{id}', 'UserController@edit')->name('user.edit')->middleware('can:user-edit');
		Route::post('update/{id}', 'UserController@update')->name('user.update')->middleware('can:user-edit');

		Route::get('delete/{id}', 'UserController@destroy')->name('user.delete')->middleware('can:user-delete');
	});

	Route::group(['prefix' => 'roles'],function(){
		Route::get('index', 'RolesController@index')->name('roles.index')->middleware('can:roles-list');

		Route::get('create', 'RolesController@create')->name('roles.create')->middleware('can:roles-create');
		Route::post('store', 'RolesController@store')->name('roles.store')->middleware('can:roles-create');

		Route::get('edit/{id}', 'RolesController@edit')->name('roles.edit')->middleware('can:roles-edit');
		Route::post('update/{id}', 'RolesController@update')->name('roles.update')->middleware('can:roles-edit');

		Route::get('delete/{id}', 'RolesController@destroy')->name('roles.delete')->middleware('can:roles-delete');
	});

	Route::group(['prefix' => 'permission'],function(){
		Route::get('create', 'PermissionController@create')->name('permission.create')->middleware('can:permission-create');
		Route::post('store', 'PermissionController@store')->name('permission.store')->middleware('can:permission-create');

		});


	Route::group(['prefix' => 'slide'], function(){
		Route::get('index', 'SlideController@index')->name('slide.index')->middleware('can:slide-list');

		Route::get('create', 'SlideController@create')->name('slide.create')->middleware('can:slide-create');
		Route::post('store','SlideController@store')->name('slide.store')->middleware('can:slide-create');

		Route::get('edit/{id}', 'SlideController@edit')->name('slide.edit')->middleware('can:slide-edit');
		Route::post('update/{id}', 'SlideController@update')->name('slide.update')->middleware('can:slide-edit');

		Route::get('delete/{id}', 'SlideController@destroy')->name('slide.delete')->middleware('can:slide-delete');
	});

});

//Route dành cho Frontend--------------------------------------
Route::get('trangchu', 'PagesController@trangchu')->name('trangchu');
Route::get('lienhe', 'PagesController@lienhe')->name('lienhe');
Route::get('loaitin/{id}/{TenKhongDau}.html', 'PagesController@loaitin')->name('loaitin');
Route::get('tintuc/{id}/{TieuDeKhongDau}.html', 'PagesController@tintuc')->name('tintuc');
Route::match(['get','post'],'timkiem', 'PagesController@PostTimKiem')->name('timkiem'); //Tìm hiểu thêm về route match,any


//Login
Route::get('dangnhap','PagesController@Login')->name('dangnhap')->middleware('UserLoggedMiddleware');
Route::post('postdangnhap','PagesController@PostLogin')->name('dangnhap.post');
Route::get('dangxuat','PagesController@Logout')->name('dangxuat');

