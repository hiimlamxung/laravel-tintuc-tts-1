<?php 
	return [
		'access' => [
			'list_theloai'		=>	'list_theloai',
			'create_theloai'	=>	'create_theloai',
			'edit_theloai'		=>	'edit_theloai',
			'delete_theloai'	=>	'delete_theloai',

			'list_loaitin'		=>	'list_loaitin',
			'create_loaitin'	=>	'create_loaitin',
			'edit_loaitin'		=>	'edit_loaitin',
			'delete_loaitin'	=>	'delete_loaitin',

			'list_tintuc'		=>	'list_tintuc',
			'create_tintuc'		=>	'create_tintuc',
			'edit_tintuc'		=>	'edit_tintuc',
			'delete_tintuc'		=>	'delete_tintuc',

			'list_user'			=>	'list_user',
			'create_user'		=>	'create_user',
			'edit_user'			=>	'edit_user',
			'delete_user'		=>	'delete_user',

			'list_slide'		=>	'list_slide',
			'create_slide'		=>	'create_slide',
			'edit_slide'		=>	'edit_slide',
			'delete_slide'		=>	'delete_slide',
			
			'list_roles'		=>	'list_roles',
			'create_roles'		=>	'create_roles',
			'edit_roles'		=>	'edit_roles',
			'delete_roles'		=>	'delete_roles',

			'list_permission'	=>	'list_permission',
			'create_permission'	=>	'create_permission',
			'edit_permission'	=>	'edit_permission',
			'delete_permission'	=>	'delete_permission',
		],

		'table_module'	=> [
			'theloai', 'loaitin', 'tintuc','user','slide','roles','permission','xxx'
		],

		'module_childrent'	=>	[
			'list','create','edit','delete'
		]
	];
 ?> 