<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use App\Policies\TheLoaiPolicy;
use App\Models\TheLoaiModel;
use App\Policies\LoaiTinPolicy;
use App\Models\LoaiTinModel;
use App\Policies\TinTucPolicy;
use App\Models\TinTucModel;
use App\Policies\RolePolicy;
use App\Models\RoleModel;
use App\Policies\UserPolicy;
use App\User;
use App\Models\SlideModel;
use App\SlidePolicy;
use App\Models\PermissionModel;
use App\PermissionPolicy;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
        //Đăng ký policy mình tạo
        TheLoaiModel::class         => TheLoaiPolicy::class, 
        LoaiTinModel::class         => LoaiTinPolicy::class,
        TinTucModel::class          => TinTucPolicy::class,
        User::class                 => UserPolicy::class,
        RoleModel::class            => RolePolicy::class,
        SlideModel::class           => SlidePolicy::class,
        PermissionModel::class      => PermissionPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        $this->DefineGateTheLoai();

        //
        //*Note:Để k phải truyền tham số vào hàm checkPermissionAccess() một cách thủ công.Làm cách sau. Tạo 1 file trong folder config đặt tên tùy ý (permissionConfig.php). Nội dung chi tiết trong file code đó.
        //Gọi: 


        //quyền vào trang thể loại
        // Gate::define('theloai-list','App\Policies\TheLoaiPolicy@view');
        // Gate::define('theloai-create','App\Policies\TheLoaiPolicy@create');
        // Gate::define('theloai-edit','App\Policies\TheLoaiPolicy@update');
        // Gate::define('theloai-delete','App\Policies\TheLoaiPolicy@delete');

        //quyền vào trang loại tin
        Gate::define('loaitin-list','App\Policies\LoaiTinPolicy@view');
        Gate::define('loaitin-create','App\Policies\LoaiTinPolicy@create');
        Gate::define('loaitin-edit','App\Policies\LoaiTinPolicy@update');
        Gate::define('loaitin-delete','App\Policies\LoaiTinPolicy@delete');

        //quyền vào trang tin tức
        Gate::define('tintuc-list','App\Policies\TinTucPolicy@view');
        Gate::define('tintuc-create','App\Policies\TinTucPolicy@create');
        Gate::define('tintuc-edit','App\Policies\TinTucPolicy@update');
        Gate::define('tintuc-delete','App\Policies\TinTucPolicy@delete');

        //quyền vào trang user
        Gate::define('user-list','App\Policies\UserPolicy@view');
        Gate::define('user-create','App\Policies\UserPolicy@create');
        Gate::define('user-edit','App\Policies\UserPolicy@update');
        Gate::define('user-delete','App\Policies\UserPolicy@delete');

        //quyền vào trang roles
        Gate::define('roles-list','App\Policies\RolesPolicy@view');
        Gate::define('roles-create','App\Policies\RolesPolicy@create');
        Gate::define('roles-edit','App\Policies\RolesPolicy@update');
        Gate::define('roles-delete','App\Policies\RolesPolicy@delete');

        //quyền vào trang slide
        Gate::define('slide-list','App\Policies\SlidePolicy@view');
        Gate::define('slide-create','App\Policies\SlidePolicy@create');
        Gate::define('slide-edit','App\Policies\SlidePolicy@update');
        Gate::define('slide-delete','App\Policies\SlidePolicy@delete');

        //quyền vào trang permission
        Gate::define('permission-list','App\Policies\PermissionPolicy@view');
        Gate::define('permission-create','App\Policies\PermissionPolicy@create');
        Gate::define('permission-edit','App\Policies\PermissionPolicy@update');
        Gate::define('permission-delete','App\Policies\PermissionPolicy@delete');

    }

//Hoặc tách thành từng function như này cho dễ quản lý
    public function DefineGateTheLoai()
    {   
        //quyền vào trang thể loại
        Gate::define('theloai-list','App\Policies\TheLoaiPolicy@view');
        Gate::define('theloai-create','App\Policies\TheLoaiPolicy@create');
        Gate::define('theloai-edit','App\Policies\TheLoaiPolicy@update');
        Gate::define('theloai-delete','App\Policies\TheLoaiPolicy@delete');
    }

}
