<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
// use Illuminate\Database\Eloquent\SoftDeletes; // add soft delete
use Auth;

class User extends Authenticatable
{
    use Notifiable;
    // SoftDeletes; // use soft delete to active soft delete

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];



    //liên kết vs bảng Role_user
    public function roles()
    {
        return $this->belongstoMany('App\Models\RoleModel','role_user','user_id','role_id');
    }

    public function checkPermissionAccess($permissionCheck)
    {
         $roles = Auth::user()->roles;
         foreach ($roles as $r) {
            $permission = $r->permission;
            // dd($permission);
            if($permission->contains('key_code',$permissionCheck)){
                return true;
            }
         }
         return false;
    }
}
