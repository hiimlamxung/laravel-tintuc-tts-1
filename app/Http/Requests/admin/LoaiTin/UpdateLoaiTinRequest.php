<?php

namespace App\Http\Requests\admin\LoaiTin;

use Illuminate\Foundation\Http\FormRequest;

class UpdateLoaiTinRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'Ten'       => 'required|min:2|max:100',
            'idTheLoai' => 'required',
        ];
    }

    public function messages()
    {
        return [
            //
            'Ten.required'          =>  'Bạn chưa nhập tên',
            'Ten.unique'            =>  'Tên loại tin đã tồn tại',
            'Ten.min'               =>  'Tên loại tin phải từ 2-100 ký tự',
            'Ten.max'               =>  'Tên loại tin phải từ 2-100 ký tự',
            'idTheLoai.required'    =>  'Bạn chưa chọn thể loại',
        ];
    }
}
