<?php

namespace App\Http\Requests\admin\User;

use Illuminate\Foundation\Http\FormRequest;

class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //'name'          =>  'required|min:3',
            'email'         =>  'required|email|unique:users,email',
            'password'      =>  'required|min:6|max:12',
            'repassword'    =>  'required|same:password',
        ];
    }

    public function messages()
    {
        return [
           'name.required'         =>  'Bạn chưa nhập tên',
           'name.min'              =>  'Tên tối thiểu 3 ký tự',
           'email.required'        =>  'Bạn chưa nhập Email',
           'email.unique'          =>  'Email đã tồn tại trên hệ thống',
           'email.email'           =>  'Bạn chưa nhập đúng định dạng Email',
           'password.required'     =>  'Bạn chưa nhập password',
           'password.min'          =>  'Mật khẩu tối thiểu 6-12 ký tự',
           'password.max'          =>  'Mật khẩu tối thiểu 6-12 ký tự',
           'repassword.required'   =>  'Bạn chưa nhập lại password',
           'repassword.same'       =>  'Mật khẩu nhập lại chưa khớp',
       ];
   }
}
