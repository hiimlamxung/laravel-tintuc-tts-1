<?php

namespace App\Http\Requests\admin\TheLoai;

use Illuminate\Foundation\Http\FormRequest;

class UpdateTheLoaiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'Ten'  => 'required|unique:TheLoai,Ten|min:2|max:100',
        ];
    }

    public function messages()
    {
        return [
            //
            'Ten.required'  => 'Bạn chưa nhập tên thể loại',
            'Ten.min'       => 'Tên thể loại phải có độ dài từ 2-100 ký tự',
            'Ten.max'       => 'Tên thể loại phải có độ dài từ 2-100 ký tự',
            'Ten.unique'    => 'Tên thể loại đã tồn tại',
        ];
    }
}
