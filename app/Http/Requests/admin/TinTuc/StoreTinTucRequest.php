<?php

namespace App\Http\Requests\admin\TinTuc;

use Illuminate\Foundation\Http\FormRequest;

class StoreTinTucRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'idLoaiTin'     =>  'required',
            'TieuDe'        =>  'required|min:2|unique:TinTuc,TieuDe|',
            'TomTat'        =>  'required',
            'NoiDung'       =>  'required',
            'Hinh'          =>  'mimes:jpeg,jpg,png | max:10000000',
        ];
    }

    public function messages()
    {
        return [
            'idLoaiTin.required'  =>  'Bạn chưa chọn loại tin',
            'TieuDe.required'     =>  'Bạn chưa nhập tiêu đề',
            'TieuDe.min'          =>  'Tiêu đề tối thiểu 2 ký tự',
            'TieuDe.unique'       =>  'Tiêu đề đã tồn tại',
            'TomTat.required'     =>  'Bạn chưa nhập tóm tắt',
            'NoiDung.required'    =>  'Bạn chưa nhập nội dung',
            'Hinh.mimes'  =>  'Đuôi ảnh phải là jpeg,jpg,png',
            'Hinh.max'    =>  'Dung lượng ảnh tối thiểu 10000000 bytes',
        ];
    }
}
