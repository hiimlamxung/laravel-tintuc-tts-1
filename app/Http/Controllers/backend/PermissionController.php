<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PermissionModel;
use DB;
class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        return view('admin.permission.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $module_parent      = $request->module_parent;
        $module_childrent   = $request->module_childrent;

        $get_module_parent = PermissionModel::where('name',$module_parent)->first(); //Lấy thông tin permision cha trong bảng

        try{
            DB::beginTransaction();
                    //Nếu permision cha này chưa tồn tại thì tạo mới 
            if(!$get_module_parent){
                $permission_parent = new PermissionModel;
                $permission_parent->name           = $request->module_parent;
                $permission_parent->display_name   = $request->module_parent;
                $permission_parent->parent_id      = 0;
                $permission_parent->key_code       = '';
                $permission_parent->save();
            }else{
                $id_module_parent = $get_module_parent->id; //Lấy ra Id của permission cha đó

                //Nếu đã tồn tại 1 record trùng name và parent_id thì ngừng, thông báo
                foreach ($module_childrent as $value) {
                    $checkDuplicate = DB::table('permission')->where('name',$value)->where('parent_id', $id_module_parent)->get();
                    if(count($checkDuplicate) > 0){
                        return redirect()->route('permission.create')->withErrors('Chức năng '. $value.' của module '. $module_parent.' đã tồn tại !');
                    }
                }
            }

        //Thêm các permission con
            foreach ($request->module_childrent as  $value) {
                $permission = new PermissionModel;
                $permission->name           = $value;
                $permission->display_name   = $value;
                if(isset($permission_parent)){
                    $permission->parent_id  = $permission_parent->id;
                }else{
                    $permission->parent_id  = $id_module_parent;
                }
                $permission->key_code       = $value.'_'.$request->module_parent;
                $permission->save();
            }
            DB::commit();
        }catch(Exception $e){
            DB::rollBack();
            return redirect()->route('permission.create')->withErrors('Lỗi: '.$e->getMessage().' Line:'. $e->getLine());
        }

        return redirect()->route('permission.create')->with('thongbao','Thêm thành công');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

//