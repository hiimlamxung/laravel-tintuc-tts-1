<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\LoaiTinModel;
use App\Models\TheLoaiModel;
use App\Models\TinTucModel;
use DB;
use Illuminate\Support\Str; //thư viện String

use App\Http\Requests\admin\LoaiTin\StoreLoaiTinRequest; //use validate form
use App\Http\Requests\admin\LoaiTin\UpdateLoaiTinRequest;

class LoaiTinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $loaitin = LoaiTinModel::all();
        return view('admin.loaitin.index',['loaitin' => $loaitin]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $theloai = TheLoaiModel::all();
        return view('admin.loaitin.create',['theloai' => $theloai]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreLoaiTinRequest $request)
    {
        //
        //
        DB::beginTransaction();
        try{
            $loaitin                = new LoaiTinModel();
            $loaitin->Ten           = $request->Ten;
            $loaitin->TenKhongDau   = Str::slug($request->Ten,'-');
            $loaitin->idTheLoai     = $request->idTheLoai;
            $loaitin->save();
            DB::commit();
        }catch(\Exception $e){
            return redirect()->route('loaitin.create')->withErrors('Thêm thất bại');
        }

        return redirect()->route('loaitin.create')->with('thongbao','Thêm loại tin thành công');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $loaitin = LoaiTinModel::find($id);
        if(!$loaitin){
            return redirect('admin/loaitin/index')->withErrors('Không tìm thấy loại tin này');
        }
        session()->put('Memory_TenLoaiTin',$loaitin->Ten); //Tạo ss lưu trữ tên loại tin cũ. Dùng cho việc sửa - Người dùng k thay đổi tên loại tin -> vẫn cho sửa
        $theloai = TheLoaiModel::all();
        return view('admin.loaitin.edit',['loaitin' => $loaitin, 'theloai' => $theloai]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateLoaiTinRequest $request, $id)
    {
        //
        $loaitin = LoaiTinModel::find($id);
        if(!$loaitin){
            return redirect('admin/loaitin/index')->withErrors('Không tìm thấy loại tin này');
        }
        //Check xem tên loại tin đã tồn tại chưa.trường hợp giữ nguyên tên loại tin -> vẫn cho thực hiện sửa.
        $check = LoaiTinModel::where('Ten','=',$request->Ten)->count();
        if($check == 0 || ($check == 1 && $request->Ten == session('Memory_TenLoaiTin'))){
            try{
                DB::beginTransaction();
                $loaitin->Ten           = $request->Ten;
                $loaitin->TenKhongDau   = Str::slug($request->Ten,'-');
                $loaitin->idTheLoai     = $request->idTheLoai;

                $loaitin->save();
                DB::commit();
            }catch(\Exception $e){
                DB:rollBack();
                return back()->withErrors('Cập nhật thất bại !');
            }
            return redirect()->route('loaitin.edit',['id' => $loaitin->id])->with('thongbao','Sửa thành công');

        }else{
            return redirect()->route('loaitin.edit',['id' => $id])->withErrors('Tên loại tin này đã tồn tại');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $loaitin  = LoaiTinModel::find($id);
        if(!$loaitin){
            return redirect('admin/loaitin/index')->withErrors('Không tìm thấy loại tin này');
        }
        try{
        //Xoá tin tức trước
            $tintuc = DB::table('tintuc')->where('idLoaiTin',$id)->delete();

        //Xóa loại tin
            $loaitin->delete();
        }catch(\Exception $e){
            return redirect('admin/loaitin/index')->withErrors('Xoá thất bại');
        }

        return redirect('admin/loaitin/index')->with('thongbao','Xoá loại tin thành công');
    }
}
