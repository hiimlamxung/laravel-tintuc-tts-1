<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SlideMoDel;

class SlideController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $slide = SlideMoDel::all();
        return view('admin.slide.index',['slide' => $slide]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.slide.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,
            [
                'Ten'       =>  'required',
                'NoiDung'   =>  'required',
                'link'      =>  'required',
            ],
            [
                'Ten.required'      =>  'Bạn chưa nhập tên',
                'NoiDung.required'  =>  'Bạn chưa nhập nội dung',
                'link.required'     =>  'Bạn chưa nhập link',
            ]);
        $slide = new SlideMoDel();
        $slide->Ten         = $request->Ten;
        $slide->NoiDung     = $request->NoiDung;
        $slide->link        = $request->link;

        if($request->hasFile('Hinh')){
            $this->validate($request,
            [
                'Hinh'     =>  'mimes:jpeg,jpg,png | max:10000000',
            ],
            [
                'Hinh.mimes'  =>  'Đuôi ảnh phải là jpeg,jpg,png',
                'Hinh.max'    =>  'Dung lượng ảnh tối thiểu 10000000 bytes',
            ]);

            $file       = $request->file('Hinh');
            $name       = $file->getClientOriginalName();//Lay ten hinh
            $new_name   = time()."_".$name;
            $file->move('upload/slide', $new_name);

            $slide->Hinh = $new_name;
        }else{
            $slide->Hinh = "";
        }

        $slide->save();
        return redirect()->route('slide.create')->with('thongbao','Thêm  thành công');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $slide = SlideMoDel::find($id);

        return view('admin.slide.edit',['slide' => $slide]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request,
            [
                'Ten'       =>  'required|unique:slide,Ten',
                'NoiDung'   =>  'required',
                'link'      =>  'required',
            ],
            [
                'Ten.required'      =>  'Bạn chưa nhập tên',
                'Ten.unique'        =>  'Tên slide đã tồn tại',
                'NoiDung.required'  =>  'Bạn chưa nhập nội dung',
                'link.required'     =>  'Bạn chưa nhập link',
            ]);
        $slide = SlideMoDel::find($id);
        $slide->Ten         = $request->Ten;
        $slide->NoiDung     = $request->NoiDung;
        $slide->link        = $request->link;

        if($request->hasFile('Hinh')){
            $this->validate($request,
            [
                'Hinh'     =>  'mimes:jpeg,jpg,png | max:10000000',
            ],
            [
                'Hinh.mimes'  =>  'Đuôi ảnh phải là jpeg,jpg,png',
                'Hinh.max'    =>  'Dung lượng ảnh tối thiểu 10000000 bytes',
            ]);

            //Xóa hình cũ
            if(file_exists('upload/slide/'.$slide->Hinh)){
                unlink('upload/slide/'.$slide->Hinh);
            }

            $file       = $request->file('Hinh');
            $name       = $file->getClientOriginalName();//Lay ten hinh
            $new_name   = time()."_".$name;
            $file->move('upload/slide', $new_name);

            $slide->Hinh = $new_name;
        }

        $slide->save();
        return redirect()->route('slide.edit',['id' => $id])->with('thongbao','Sửa thành công');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slide = SlideMoDel::find($id);

        //Xóa hình cũ
            if(file_exists('upload/slide/'.$slide->Hinh)){
                unlink('upload/slide/'.$slide->Hinh);
            }

        $slide->delete();
        return redirect()->route('slide.index')->with('thongbao','Xoá thành công');
    }
}
