<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TheLoaiModel;
use Illuminate\Support\Str; //Thêm thư viện String.
use DB;
use App\Http\Requests\admin\TheLoai\StoreTheLoaiRequest;
use App\Http\Requests\admin\TheLoai\UpdateTheLoaiRequest;

class TheLoaiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $theloai = TheLoaiModel::all();
        return view('admin.theloai.index', ['theloai' => $theloai]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.theloai.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTheLoaiRequest $request)
    {
        try{
            DB::beginTransaction();

            $theloai                = new TheLoaiModel();
            $theloai->Ten           = $request->Ten;
            $theloai->TenKhongDau   = Str::slug($request->Ten,'-');
            $theloai->save();
            DB::commit();
            return redirect()->route('theloai.create')->with('thongbao','Thêm thành công');
        }catch(\Exception $e){
            DB::rollBack();

            return redirect()->route('theloai.create')->withErrors('Thêm thất bại. Đã xảy ra lỗi!');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(TheLoaiModel $id)
    {   

        return view('admin.theloai.edit',['theloai' => $id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTheLoaiRequest $request, $id)
    {
        //
        try{
            DB::beginTransaction();
            $theloai = TheLoaiModel::find($id);
            if(!$theloai){
                return redirect()->route('theloai.index')->withErrors('Không tìm thấy thể loại');
            }

            $theloai->Ten           = $request->Ten;
            $theloai->TenKhongDau   = Str::slug($request->Ten,'-');
            $theloai->save();
            DB::commit();

        }catch(\Exception $e){
            DB::rollBack();
            return redirect()->route('theloai.edit',['id' => $id])->withErrors('Cập nhật không thành công !');
        }
        return redirect()->route('theloai.edit',['id' => $id])->with('thongbao','Sửa thành công');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $theloai = TheLoaiModel::find($id);
        if(!$theloai){
            return redirect('admin/theloai/index')->withErrors('Không tìm thấy thể loại này!');
        }
        try{
            DB::beginTransaction();
             //Xoá tin tức thuộc thể loại này
            $tintuc = TheLoaiModel::find($id)->tintuc;
            $del_tintuc = DB::table('tintuc');
            foreach ($tintuc as  $value) {
                $del_tintuc->orWhere('id',$value->id);
            }
            $del_tintuc->delete();

            //Xóa Loại tin thuộc thể loại này
            $loaitin = TheLoaiModel::find($id)->loaitin;
            $del_loaitin = DB::table('loaitin');
            foreach ($loaitin as  $value) {
                $del_loaitin->orWhere('id',$value->id);
            }
            $del_loaitin->delete();

            //Xóa thể loại
            $theloai->delete();

            DB::commit();
            return redirect('admin/theloai/index')->with('thongbao','Bạn đã xóa thành công');
        }catch(\Exception $e){
            DB::rollBack();
            return redirect('admin/theloai/index')->withErrors('Có lỗi trong quá trình xóa !');
        }     
    }
}
