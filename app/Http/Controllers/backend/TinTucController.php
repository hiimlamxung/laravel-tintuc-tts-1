<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TinTucModel;
use App\Models\TheLoaiModel;
use App\Models\LoaiTinModel;
use Illuminate\Support\Str;
use App\Http\Requests\admin\TinTuc\StoreTinTucRequest;
use App\Http\Requests\admin\TinTuc\UpdateTinTucRequest;
use DB;

class TinTucController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tintuc = TinTucModel::all();
        return view('admin.tintuc.index',['tintuc' => $tintuc]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $theloai = TheLoaiModel::all();
        $loaitin = LoaiTinModel::all();

        return view('admin.tintuc.create',['theloai' => $theloai, 'loaitin' => $loaitin]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTinTucRequest $request)
    {
        //

        try{
            DB::beginTransaction();
            $tintuc                 = new TinTucModel();

            $tintuc->TieuDe         = $request->TieuDe;
            $tintuc->TieuDeKhongDau = Str::slug($request->TieuDe,'-');
            $tintuc->idLoaiTin      = $request->idLoaiTin;
            $tintuc->TomTat         = $request->TomTat;
            $tintuc->NoiDung        = $request->NoiDung;
            $tintuc->NoiBat         = $request->NoiBat;

        //Hình ảnh
            if($request->hasFile('Hinh')){
                $file       = $request->file('Hinh');
                $name       = $file->getClientOriginalName(); //tên hình upload
                $new_name   = time()."_".$name; //Đặt tên mới cho hình

                $file->move('upload/tintuc',$new_name);
                $tintuc->Hinh = $new_name;
                
            }else{
                $tintuc->Hinh = '';
            }
            $tintuc->save();
            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return redirect()->route('tintuc.create')->withErrors('Thêm thất bại !');
        }

        return redirect()->route('tintuc.create')->with('thongbao','Thêm tin tức thành công');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $tintuc  = TinTucModel::find($id);
        if(!$tintuc){
            return redirect()->route('tintuc.index')->withErrors('Không tìm thấy bài tin tức trên');
        }
        $theloai = TheLoaiModel::all();
        $loaitin = LoaiTinModel::all();
        return view('admin.tintuc.edit',['tintuc' => $tintuc, 'theloai' => $theloai, 'loaitin' => $loaitin]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTinTucRequest $request, $id)
    {
        try{
            DB::beginTransaction();
            $tintuc = TinTucModel::find($id);
            $tintuc->TieuDe         = $request->TieuDe;
            $tintuc->TieuDeKhongDau = Str::slug($request->TieuDe,'-');
            $tintuc->idLoaiTin      = $request->idLoaiTin;
            $tintuc->TomTat         = $request->TomTat;
            $tintuc->NoiDung        = $request->NoiDung;
            $tintuc->NoiBat         = $request->NoiBat;

            //Hình ảnh
            if($request->hasFile('Hinh')){
            //Xóa ảnh cũ
                if(file_exists('upload/tintuc/'.$tintuc->Hinh)){
                    unlink('upload/tintuc/'.$tintuc->Hinh);
                }

                $file       = $request->file('Hinh');
            $name       = $file->getClientOriginalName(); //tên hình upload
            $new_name   = time()."_".$name; //Đặt tên mới cho hình

            $file->move('upload/tintuc',$new_name);
            $tintuc->Hinh = $new_name;   
        }
        $tintuc->save();
        DB::commit();
    }catch(\Exception $e){
        DB::rollBack();
        return redirect('admin/tintuc/index')->withErrors('Sửa thất bại');
    }
    return redirect()->route('tintuc.edit',['id' => $tintuc->id])->with('thongbao','Sửa thành công');

}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try{
            DB::beginTransaction();
            $tintuc = TinTucModel::find($id);
        //Xóa ảnh cũ
            if(file_exists('upload/tintuc/'.$tintuc->Hinh && $tintuc->Hinh!= '')){
                return 'đây là file hình';
            }
            $tintuc->delete();
            DB::commit();

        }catch(\Exception $e){
            DB::rollBack();
            return redirect('admin/tintuc/index')->withErrors('Xoá thất bại');
        }
        return redirect('admin/tintuc/index')->with('thongbao','Xoá thành công');
    }
}

