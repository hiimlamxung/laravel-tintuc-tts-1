<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\RoleModel;
use App\Models\PermissionModel;
use Illuminate\Support\Facades\Config;
use DB;

class RolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $roles = RoleModel::all();
        return view('admin.roles.index',compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $permissionParent = PermissionModel::where('parent_id',0)->with('permissionChildrent')->get();
        return view('admin.roles.create',compact('permissionParent'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $role = new RoleModel;
        $role->name         = $request->name;
        $role->display_name = $request->display_name;
        $role->save();

        $role->permission()->attach($request->permission_id);
        return redirect()->route('roles.create')->with('thongbao','Thêm thành công');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $permissionParent = PermissionModel::where('parent_id',0)->with('permissionChildrent')->get();
        $role = RoleModel::find($id);
        $permissionChecked = $role->permission;

        // dd($permissionChecked);
        return view('admin.roles.edit',compact(['role','permissionParent','permissionChecked']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $role = RoleModel::find($id);
        $role->name         = $request->name;
        $role->display_name = $request->display_name;
        $role->save();

        $role->permission()->sync($request->permission_id);
        return redirect()->route('roles.edit',['id' => $id])->with('thongbao','Sửa thành công');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        DB::table('role_user')->where('role_id',$id)->delete();
        $role = RoleModel::find($id)->delete();
        return redirect()->route('roles.index')->with('thongbao','Xoá thành công !');
    }

}
