<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TinTucModel;
use App\Models\TheLoaiModel;
use App\Models\LoaiTinModel;
use Illuminate\Support\Str;

class AjaxController extends Controller
{
    //
    public function getLoaiTin($idTheLoai)
    {
    	$loaitin = LoaiTinModel::where('idTheLoai','=',$idTheLoai)->get();
    	foreach ($loaitin as $key => $lt) {
    		echo "<option value='".$lt->id."''>".$lt->Ten."</option>";
    	}
    }
}
