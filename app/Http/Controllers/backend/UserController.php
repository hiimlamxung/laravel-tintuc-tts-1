<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Str;
use Auth;
use DB;
use App\Models\RoleModel;
use App\Http\Requests\admin\User\StoreUserRequest;
use App\Http\Requests\admin\User\UpdateUserRequest;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $user = User::all();
        return view('admin.user.index',['user' => $user]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $role = RoleModel::all();
        return view('admin.user.create',['role' => $role]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserRequest $request)
    {
        //
        //Dùng try catch để bảo đảm tính toàn vẹn dữ liệu.Kiểm tra mọi câu truy vấn phải thực hiện đúng thì mới hành động.Cách này đảm bảo mọi hành động chắc chắn thành công thì mới dám thực hiện
        try{
            DB::beginTransaction(); //// Bắt đầu các hành động trên CSDL
            $user       = new User();
            $user->name     = $request->name;
            $user->email    = $request->email;
            $user->password = bcrypt($request->password);
            $user->quyen    = $request->quyen;
            $user->save();

            if($request->quyen == 1){
                $roles_id = $request->role_id;
            //Bình thường sẽ foreach rồi insert,nhưng laravel có hàm attach hỗ trợ thêm dữ liệu vào bảng trung gian.
                $user->roles()->attach($roles_id);
            } 
            DB::commit(); //Commit dữ liệu khi hoàn thành kiểm tra
        }catch(Exception $exception){
            DB::rollBack(); //rollback lại hành động trc đó
            redirect('admin/user/create')->withErrors('Thêm thất bại');
        }
        return redirect('admin/user/create')->with('thongbao','Thêm thành công');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // try{
        $user = User::find($id);
        $role = RoleModel::all();
        $roleofUser =  $user->roles;
        // }catch(\Exception $e){
        //     return view('admin.user.index')->withErrors('Không tìm thấy đối tượng');
        // }
        return view('admin.user.edit',['user' => $user, 'role' => $role,'roleofUser' => $roleofUser]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, $id)
    {
        //
        try{
            DB::beginTransaction();
            $user = User::find($id);
            $user->name     = $request->name;
            $user->quyen    = $request->quyen;

            if($request->changepass){
                $user->password = bcrypt($request->password);
                $this->validate($request,
                    [
                        'password'      =>  'required|min:6|max:12',
                        'repassword'    =>  'required|same:password',
                    ],
                    [
                        'password.required'     =>  'Bạn chưa nhập password',
                        'password.min'          =>  'Mật khẩu tối thiểu 6-12 ký tự',
                        'password.max'          =>  'Mật khẩu tối thiểu 6-12 ký tự',
                        'repassword.required'   =>  'Bạn chưa nhập lại password',
                        'repassword.same'       =>  'Mật khẩu nhập lại chưa khớp',
                    ]);
            }

            $user->save();
            $user->roles()->sync($request->role_id);
            DB::commit();
        }catch(\Exception $e){
            DB::rollBack();
            return redirect('admin/user/index/')->withErrors('Sửa thất bại');
        }
        return redirect('admin/user/edit/'.$user->id)->with('thongbao','Sửa thành công');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try{
            DB::beginTransaction();
            DB::table('role_user')->where('user_id','=',$id)->delete();
            $user = User::find($id);

            $user->delete();
            DB::commit();
        }catch(\Exception $e){
            DB::rollBack();
            return redirect('admin/user/index')->withErrors('Xoá thất bại');
        }
        return redirect('admin/user/index')->with('thongbao','Xoá thành công');
    }

    public function LoginAdmin()
    {

        return view('admin.login');
    }

    public function PostLoginAdmin(Request $request)
    {   

        $this->validate($request,
            [
                'email'     =>  'required|email',
                'password'  =>  'required|min:6|max:12',
            ],
            [
                'email.required'    => 'Bạn chưa nhập email',
                'email.email'       => 'Bạn nhập chưa đúng email',
                'password.required' => 'Bạn chưa nhập mật khẩu',
                'password.min'      => 'Mật khẩu tối thiếu 6-12 ký tự',
                'password.max'      => 'Mật khẩu tối thiếu 6-12 ký tự',
            ]);
        if($request->has('remember_me')){
            $remember = true; //user có tích remember me
        }else{
            $remember = false;
        }
        if(Auth::attempt(['email' => $request->email,'password' => $request->password], $remember)){
            return redirect('admin/theloai/index');
        }else{
            return redirect('admin/login')->with('thongbao','Đăng nhập thất bại');
        }
    }

    public function LogoutAdmin()
    {
        Auth::logout();
        return redirect()->route('login.admin');
    }
}
