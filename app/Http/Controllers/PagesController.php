<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TheLoaiModel;
use App\Models\LoaiTinModel;
use App\Models\TinTucModel;
use App\Models\SlideModel;
use Illuminate\Support\Str;
use Auth;

class PagesController extends Controller
{
    //
    function __construct()
    {
    	$theloai 	= TheLoaiModel::all();
    	$slide 		= SlideModel::all();
    	view()->share(['theloai' => $theloai, 'slide' => $slide]); //share biến,những view được gọi từ PagesController đều sử dụng đc
    }
    public function trangchu()
    {
    	// $theloai = TheLoaiModel::all();
    	return view('pages.trangchu');
    }

    public function lienhe()
    {
    	// $theloai = TheLoaiModel::all();
    	return view('pages.lienhe');
    }

    public function loaitin($id, $TenKhongDau)
    {	
            $loaitin    = LoaiTinModel::where('id',$id)->where('TenKhongDau',$TenKhongDau)->first();
            $tintuc     = TinTucModel::where('idLoaiTin',$id)->paginate(5);

        return view('pages.loaitin',['loaitin' => $loaitin, 'tintuc' => $tintuc]);
    }

    public function tintuc($id, $TieuDeKhongDau)
    {   
        $this->authorize('admin-tintuc');//Phân quyền bằng gate

        $tintuc 		= TinTucModel::where('id',$id)->where('TieuDeKhongDau',$TieuDeKhongDau)->first();
        $tinnoibat 		= TinTucModel::where('NoiBat',1)->where('id','!=',$id)->take(5)->get();
        $tinlienquan 	=TinTucModel::where('idLoaiTin',$tintuc->idLoaiTin)->take(4)->get();
        return view('pages.tintuc',['tintuc' => $tintuc,'tinnoibat' => $tinnoibat, 'tinlienquan' => $tinlienquan]);
    }

    public function PostTimKiem(Request $request)
    {
    	$tukhoa = $request->tukhoa;
    	$tintuc = TinTucModel::where('TieuDe','like',"%$tukhoa%")->orWhere('TomTat','like',"%$tukhoa%")->orWhere('NoiDung','like',"%$tukhoa%")->paginate(5)->appends(['tukhoa' => $tukhoa]);
    	//appends : thêm biến ?tukhoa vào thanh url
    	return view('pages.timkiem',['tintuc' => $tintuc,'tukhoa' => $tukhoa]);
    }

    public function Login()
    {
    	return view('pages.dangnhap');
    }

    public function PostLogin(Request $request)
    {
    	$this->validate($request,
    		[
    			'email'     =>  'required|email',
                'password'  =>  'required|min:6|max:12',
            ],
            [
             'email.required'    => 'Bạn chưa nhập email',
             'email.email'       => 'Bạn nhập chưa đúng email',
             'password.required' => 'Bạn chưa nhập mật khẩu',
             'password.min'      => 'Mật khẩu tối thiếu 6-12 ký tự',
             'password.max'      => 'Mật khẩu tối thiếu 6-12 ký tự',
         ]);

    	if(Auth::attempt(['email' => $request->email,'password' => $request->password])){
            return redirect('trangchu');
        }else{
            return redirect('dangnhap')->with('thongbao','Đăng nhập thất bại');
        }
    }

    public function Logout()
    {
    	Auth::logout();
      return redirect('trangchu');
  }

    public function ShowPost($id) //Thử làm phân quyền bằng policy
    {   
        $post = \App\Models\PostModel::find($id);    
        $user = Auth::user();

        $this->authorize('view',$post);

        //or
        // if (!Auth::check() || $user->cannot('view', $post)){
        //     return view('errors.403');
        // }

        return view('pages.showpost',compact('post'));
    }
}

