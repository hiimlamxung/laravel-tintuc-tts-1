<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class UserLoggedMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    //Nếu user đã login thì chặn truy cập trang login
    public function handle($request, Closure $next)
    {   
        if(Auth::check()){
            return redirect('trangchu');
        }else{
            return $next($request);
        }
    }
}
