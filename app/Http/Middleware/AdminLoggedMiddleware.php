<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AdminLoggedMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    //Nếu đã đăng nhập,và cố tình truy cập trang login,thì đẩy về trang chủ chức năng
    public function handle($request, Closure $next)
    {
        if(!Auth::check() || (Auth::check() && Auth::user()->quyen != 1)){
            return $next($request);
        }else{
            return redirect()->route('theloai.index');
        }
        
    }
}
