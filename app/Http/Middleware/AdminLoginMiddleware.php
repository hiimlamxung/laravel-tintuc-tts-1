<?php

namespace App\Http\Middleware;

use Closure;
use Auth; //phải thêm thư viện Auth mới dùng đc thư viện Auth

class AdminLoginMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    //Nếu chưa đăng nhập,thì đẩy ra ngoài

    public function handle($request, Closure $next)
    {
        if(Auth::check()){
            $user = Auth::user();
            if($user->quyen == 1){ //quyền =1 (user là admin ms có quyền truy cập)
                return $next($request);
            }else{
                return redirect()->route('login.admin');
            }
            
        }
            return redirect()->route('login.admin');
    }
}
//Sau đó khai báo middleware này trong file kernel - phần $routeMiddleware