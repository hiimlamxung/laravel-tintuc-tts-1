<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TheLoaiModel extends Model
{
    //
    protected $table = "theloai";

    //liên kết bảng thể loại vs bảng loại tin
    public function loaitin()
    {
    	return $this->hasMany('App\Models\LoaiTinModel', 'idTheLoai', 'id');
    }

    //liên kết bảng thể loại vs bảng tin tức
    public function tintuc()
    {
    	return $this->hasManyThrough('App\Models\TinTucModel', 'App\Models\LoaiTinModel', 'idTheLoai', 'idLoaiTin', 'id');
    }
}
