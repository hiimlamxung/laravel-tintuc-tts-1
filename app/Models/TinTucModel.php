<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TinTucModel extends Model
{
    //
    protected $table = "tintuc";

    //Liên kết bảng tin tức vs bảng loại tin
    public function loaitin()
    {
    	return $this->belongsTo('App\Models\LoaiTinModel', 'idLoaiTin', 'id');
    }

    // //Liên kết bảng tin tức vs bảng comment
    // public function comment()
    // {
    // 	return $this->hasMany('App\Models\CommentModel', 'idTinTuc', 'id');
    // }
}
