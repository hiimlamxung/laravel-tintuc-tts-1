<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PermissionModel extends Model
{
    //
	protected $table = 'permission';
	// protected $guarded = [];
	// protected $fillable = ['name','display_name','parent_id','key_code'];

	public function permissionChildrent() //Liên kết với bảng chính nó,lấy cột parent_id làm khóa phụ mapping với cột id làm khóa chính
	{
		return $this->hasMany('App\Models\PermissionModel','parent_id','id');
	}
}
