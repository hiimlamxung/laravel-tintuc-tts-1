<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RoleModel extends Model
{
    //
    protected $table = 'roles';
    protected $fillable = ['name','display_name'];

    public function permission()
    {
    	return $this->belongsToMany('App\Models\PermissionModel','permission_role','role_id','permission_id');
    }

    public function user()
    {
        return $this->belongstoMany('App\User','role_user','role_id','user_id');
    }
}
