<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LoaiTinModel extends Model
{
    //
    protected $table = "loaitin";

    //liên kết bảng loại tin vs bảng thể loại
    public function theloai()
    {
    	return $this->belongsTo('App\Models\TheLoaiModel', 'idTheLoai', 'id');
    }

    //Liên kết bảng loại tin vs bảng tin tức
    public function tintuc()
    {
    	return $this->hasMany('App\Models\TinTucModel', 'idLoaiTin', 'id');
    }
}
