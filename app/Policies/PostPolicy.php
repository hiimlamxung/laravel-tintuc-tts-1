<?php

namespace App\Policies;

use App\Models\PostModel;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PostPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\Models\PostModel  $postModel
     * @return mixed
     */
    public function view(User $user, PostModel $postModel)
    {
        //
        return $user->id == $postModel->user_id;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
        return $user->checkPermissionAccess(config('permission.access.create_user'));
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\Models\PostModel  $postModel
     * @return mixed
     */
    public function update(User $user, PostModel $postModel)
    {
        //
        return $user->checkPermissionAccess(config('permission.access.edit_user'));
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Models\PostModel  $postModel
     * @return mixed
     */
    public function delete(User $user, PostModel $postModel)
    {
        //
        return $user->checkPermissionAccess(config('permission.access.delete_user'));
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\User  $user
     * @param  \App\Models\PostModel  $postModel
     * @return mixed
     */
    public function restore(User $user, PostModel $postModel)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Models\PostModel  $postModel
     * @return mixed
     */
    public function forceDelete(User $user, PostModel $postModel)
    {
        //
    }
}
