<?php

namespace App\Policies;

use App\PermissionModel;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PermissionPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\PermissionModel  $permissionModel
     * @return mixed
     */
    public function view(User $user, PermissionModel $permissionModel)
    {
        //
        return $user->checkPermissionAccess(config('permission.access.list_permission'));
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
        return $user->checkPermissionAccess(config('permission.access.create_permission'));
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\PermissionModel  $permissionModel
     * @return mixed
     */
    public function update(User $user, PermissionModel $permissionModel)
    {
        //
        return $user->checkPermissionAccess(config('permission.access.edit_permission'));
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\PermissionModel  $permissionModel
     * @return mixed
     */
    public function delete(User $user, PermissionModel $permissionModel)
    {
        //
        return $user->checkPermissionAccess(config('permission.access.delete_permission'));
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\User  $user
     * @param  \App\PermissionModel  $permissionModel
     * @return mixed
     */
    public function restore(User $user, PermissionModel $permissionModel)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\PermissionModel  $permissionModel
     * @return mixed
     */
    public function forceDelete(User $user, PermissionModel $permissionModel)
    {
        //
    }
}
