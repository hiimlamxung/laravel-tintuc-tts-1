<?php

namespace App\Policies;

use App\SlideModel;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class SlidePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\SlideModel  $slideModel
     * @return mixed
     */
    public function view(User $user)
    {
        //
        return $user->checkPermissionAccess(config('permission.access.list_slide'));
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
        return $user->checkPermissionAccess(config('permission.access.create_slide'));
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\SlideModel  $slideModel
     * @return mixed
     */
    public function update(User $user)
    {
        //
        return $user->checkPermissionAccess(config('permission.access.update_slide'));
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\SlideModel  $slideModel
     * @return mixed
     */
    public function delete(User $user)
    {
        //
        return $user->checkPermissionAccess(config('permission.access.delete_slide'));
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\User  $user
     * @param  \App\SlideModel  $slideModel
     * @return mixed
     */
    public function restore(User $user)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\SlideModel  $slideModel
     * @return mixed
     */
    public function forceDelete(User $user)
    {
        //
    }
}
