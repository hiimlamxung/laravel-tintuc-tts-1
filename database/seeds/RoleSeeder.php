<?php

use Illuminate\Database\Seeder;
use App\Models\RoleModel;
use Illuminate\Database\Eloquent\Model;

class RolseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('rolses')->insert([
        	['name' => 'admin','display_name' => 'quản trị hệ thống'],
        	['name' => 'guest','display_name' => 'khách hàng'],
        	['name' => 'dev','display_name' => 'phát triển hệ thống'],
        	['name' => 'content','display_name' => 'chỉnh sửa content'],
        ]);
    }
}
