    <script type="text/javascript">
        $(document).ready(function() {
            if($("#quyen0").is(":checked")){
                $("#role").attr('disabled','');
            }else{
                $("#role").removeAttr('disabled');
            }

//Click thay đổi mật khẩu
            $("#changepass").change(function(event) {
                if($(this).is(":checked")){
                    $(".password").removeAttr('disabled');
                }else{
                    $(".password").attr('disabled','');
                }
            });
//Cick thay đổi quyền
            $("body").on('change', '#quyen1', function(event) {
               if($(this).is(":checked")){
                    $("#role").removeAttr('disabled');
               }
            });
            $("body").on('change', '#quyen0', function(event) {
               if($(this).is(":checked")){
                    $("#role").attr('disabled','');
               }
            });
        });
    </script>