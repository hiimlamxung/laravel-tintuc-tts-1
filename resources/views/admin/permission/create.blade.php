@extends('admin.layout.index')

@section('content')
<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Permission
                            <small>Thêm mới:</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-md-12" style="padding-bottom:120px">
                        @if(count($errors) > 0)
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $value)
                                    {{ $value }} <br>
                                @endforeach
                            </div>
                        @endif

                        @if(session('thongbao') !== null)
                            <div class="alert alert-success">{{ session('thongbao') }}</div>
                        @endif
                        <form action="admin/permission/store" method="POST">
                            @csrf
                            <div class="form-group">
                                <label for="">Chọn tên Module:</label>
                                <select name="module_parent" class="form-control">
                                    @foreach(config('permission.table_module') as $moduleItem)
                                    <option value="{{ $moduleItem }}">{{ $moduleItem }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                 @foreach(config('permission.module_childrent') as $moduleItemChildrent)   
                                    <div class="col-md-3">
                                        <label for="">
                                            <input type="checkbox" name="module_childrent[]" value="{{ $moduleItemChildrent }}">{{ $moduleItemChildrent }}
                                        </label>
                                    </div>
                                @endforeach

                                </div>
                            </div>
                            <button type="submit" class="btn btn-default">Thêm mới</button>
                        </form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection