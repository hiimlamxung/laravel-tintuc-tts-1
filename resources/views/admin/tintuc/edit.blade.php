@extends('admin.layout.index')

@section('content')
<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"> Sửa tin tức <br>
                            <small>{{ $tintuc->TieuDe }}</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                        @if(count($errors) > 0)
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $value)
                                    {{ $value }} <br>
                                @endforeach
                            </div>
                        @endif

                        @if(session('thongbao') !== null)
                            <div class="alert alert-success">{{ session('thongbao') }}</div>
                        @endif
                        <form action="{{ route('tintuc.update',['id' => $tintuc->id]) }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label>Thể loại:</label>
                                <select class="form-control" name="TheLoai" id="TheLoai">
                                    @foreach($theloai as $tl)
                                    <option
                                    @if($tintuc->loaitin->theloai->id == $tl->id)
                                    selected
                                    @endif
                                     value="{{ $tl->id }}">{{ $tl->Ten }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Loại tin:</label>
                                <select class="form-control" name="idLoaiTin" id="LoaiTin">
                                    @foreach($loaitin as $lt)
                                    <option
                                    @if($tintuc->idLoaiTin == $lt->id)
                                    selected
                                    @endif
                                     value="{{ $lt->id }}">{{ $lt->Ten }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Tiêu đề:</label>
                                <input class="form-control" name="TieuDe" placeholder="Nhập tiêu đề" value="{{ $tintuc->TieuDe }}"/>
                            </div>
                            <div class="form-group">
                                <label>Tóm tắt:</label>
                                <textarea class="form-control ckeditor" id="TomTat" name="TomTat" rows="3" >{{ $tintuc->TomTat }}</textarea>
                            </div>
                            <div class="form-group">
                                <label>Nội Dung:</label>
                                <textarea class="form-control ckeditor" id="NoiDung" name="NoiDung" rows="3">{{ $tintuc->NoiDung }}</textarea>
                            </div>
                            <div class="form-group">
                                <label>Hình ảnh:</label>
                                <img src="{{ "upload/tintuc/".$tintuc->Hinh }}" alt="" style="width: 100px;">
                                <input type="file" name="Hinh" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Nổi bật:</label>
                                <label class="radio-inline">
                                    <input 
                                    @if($tintuc->NoiBat ==1)
                                    checked
                                    @endif
                                    name="NoiBat" value="1"  type="radio">Có
                                </label>
                                <label class="radio-inline">
                                    <input 
                                    @if($tintuc->NoiBat ==0)
                                    checked
                                    @endif
                                    name="NoiBat" value="0" type="radio">Không
                                </label>
                            </div>
                            <button type="submit" class="btn btn-default">Sửa</button>
                            <button type="reset" class="btn btn-default">Reset</button>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection

@section('script')
    @include('js.admin.tintuc.edit')
@endsection