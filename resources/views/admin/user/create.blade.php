@extends('admin.layout.index')

@section('content')
<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">User
                            <small>Thêm mới:</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                        @if(count($errors) > 0)
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $er)
                                {{ $er }} <br>
                                @endforeach
                            </div>
                        @endif

                        @if(session('thongbao') !== null)
                            <div class="alert alert-success">
                                {{ session('thongbao') }}
                            </div>
                        @endif
                        <form action="admin/user/store" method="POST">
                            @csrf
                            <div class="form-group">
                                <label>Tên:</label>
                                <input class="form-control" name="name" placeholder="Tên user" type="text" />
                            </div>
                            <div class="form-group">
                                <label>Email:</label>
                                <input class="form-control" name="email" placeholder="Email" type="email" />
                            </div>
                            <div class="form-group">
                                <label>Password</label>
                                <input type="password" class="form-control" name="password" placeholder="Nhập mật khẩu" />
                            </div>
                            <div class="form-group">
                                <label>Nhập lại Password</label>
                                <input type="password" class="form-control" name="repassword" placeholder="Nhập lại mật khẩu" />
                            </div>
                            <div class="form-group">
                                <label>Chọn loại người dùng</label>
                                <label class="radio-inline">
                                    <input name="quyen" id="quyen0" value="0" checked="" type="radio">Thường
                                </label>
                                <label class="radio-inline">
                                    <input name="quyen" id="quyen1" value="1" type="radio">Thành viên
                                </label>
                            </div>
                            <div class="form-group">
                                <label for="">Chọn vai trò</label>
                                <select name="role_id[]" id="role" class="form-control" multiple="" disabled="">
                                    @foreach($role as $r)
                                    <option value="{{ $r->id }}">{{ $r->name }}</option>
                                    @endforeach
                                    
                                </select>
                            </div>
                            <button type="submit" class="btn btn-default">Thêm mới</button>
                            <button type="reset" class="btn btn-default">Reset</button>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection

@section('script')
    @include('js.admin.user.create')
@endsection