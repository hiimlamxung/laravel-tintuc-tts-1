@extends('admin.layout.index')

@section('content')
<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">User {{ $user->name }}
                            <small>Sửa:</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                        @if(count($errors) > 0)
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $er)
                                {{ $er }} <br>
                                @endforeach
                            </div>
                        @endif

                        @if(session('thongbao') !== null)
                            <div class="alert alert-success">
                                {{ session('thongbao') }}
                            </div>
                        @endif
                        <form action="admin/user/update/{{ $user->id }}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label>Tên:</label>
                                <input class="form-control" name="name" placeholder="Tên user" type="text" value="{{ $user->name }}" />
                            </div>
                            <div class="form-group">
                                <label>Email:</label>
                                <input class="form-control" name="email" placeholder="Email" type="email" value="{{ $user->email }}" readonly="" />
                            </div>
                            <div class="form-group">
                                <input type="checkbox" name="changepass" id="changepass">
                                <label>Đổi mật khẩu</label>
                                <input type="password" class="form-control password" name="password" disabled="" placeholder="Nhập mật khẩu" />
                            </div>
                            <div class="form-group">
                                <label>Nhập lại mật khẩu</label>
                                <input type="password" class="form-control password" name="repassword" disabled="" placeholder="Nhập lại mật khẩu" />
                            </div>
                            <div class="form-group">
                                <label>Chọn loại người dùng</label>
                                <label class="radio-inline quyen">
                                    <input
                                    @if($user->quyen == 0)
                                    checked
                                    @endif
                                     name="quyen" value="0" type="radio" id="quyen0">Thường
                                </label>
                                <label class="radio-inline quyen">
                                    <input
                                    @if($user->quyen == 1)
                                    checked
                                    @endif
                                     name="quyen" value="1" type="radio" id="quyen1">Admin
                                </label>
                            </div>
                            <div class="form-group">
                                <label for="">Chọn vai trò</label>
                                <select name="role_id[]" id="role" class="form-control" multiple="" disabled="">
                                    @foreach($role as $r)
                                    <option
                                    @if($roleofUser->contains('id',$r->id))
                                    selected
                                    @endif
                                     value="{{ $r->id }}">{{ $r->name }}</option>
                                    @endforeach
                                    
                                </select>
                            </div>
                            <button type="submit" class="btn btn-default">Sửa</button>
                            <button type="reset" class="btn btn-default">Reset</button>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection

@section('script')
@include('js.admin.user.edit')
@endsection