<div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                            <!-- /input-group -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="admin/theloai/index"><i class="fa fa-bar-chart-o fa-fw"></i> Thể loại<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                @can('theloai-list')
                                <li>
                                    <a href="admin/theloai/index">Danh sách</a>
                                </li>
                                @endcan
                                @can('theloai-create')
                                <li>
                                    <a href="{{ route('theloai.create') }}">Thêm mới</a>
                                </li>
                                @endcan
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-cube fa-fw"></i> Loại tin<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                @can('loaitin-list')
                                <li>
                                    <a href="admin/loaitin/index">Danh sách</a>
                                </li>
                                @endcan
                                @can('loaitin-create')
                                <li>
                                    <a href="{{ route('loaitin.create') }}">Thêm mới</a>
                                </li>
                                @endcan
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-users fa-fw"></i> Tin tức<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                @can('tintuc-list')
                                <li>
                                    <a href="admin/tintuc/index">Danh sách</a>
                                </li>
                                @endcan
                                @can('tintuc-create')
                                <li>
                                    <a href="{{ route('tintuc.create') }}">Thêm mới</a>
                                </li>
                                @endcan
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-users fa-fw"></i> User<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                @can('user-list')
                                <li>
                                    <a href="admin/user/index">Danh sách</a>
                                </li>
                                @endcan
                                @can('user-create')
                                <li>
                                    <a href="{{ route('user.create') }}">Thêm mới</a>
                                </li>
                                @endcan
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-users fa-fw"></i> Slide<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="admin/slide/index">Danh sách</a>
                                </li>
                                <li>
                                    <a href="{{ route('slide.create') }}">Thêm mới</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-users fa-fw"></i>Danh sách vai trò (Roles)<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                @can('roles-list')
                                <li>
                                    <a href="admin/roles/index">Danh sách</a>
                                </li>
                                @endcan
                                @can('roles-create')
                                <li>
                                    <a href="{{ route('roles.create') }}">Thêm mới</a>
                                </li>
                                @endcan
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="{{ route('permission.create') }}"><i class="fa fa-users fa-fw"></i>Tạo chức năng Permission</a>
                            <!-- /.nav-second-level -->
                        </li>
                        
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>