@extends('admin.layout.index')

@section('content')
<style type="text/css" media="screen">
    .panel-default>.panel-heading{
        background: #337ab7;
    }
</style>
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Vai trò
                    <small>Thêm mới:</small>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
            <div class="col-lg-12" style="padding-bottom:120px">
                @if(count($errors) > 0)
                <div class="alert alert-danger">
                    @foreach($errors->all() as $er)
                    {{ $er }} <br>
                    @endforeach
                </div>
                @endif

                @if(session('thongbao') !== null)
                <div class="alert alert-success">
                    {{ session('thongbao') }}
                </div>
                @endif
                <form action="admin/roles/store" method="POST">
                    @csrf
                    <div class="form-group">
                        <label>Tên vai trò:</label>
                        <input class="form-control" name="name" placeholder="Tên vai trò" type="text" />
                    </div>
                    <div class="form-group">
                        <label>Mô tả:</label>
                        <textarea class="form-control" name="display_name" id="" cols="30" rows="5"></textarea>
                    </div>
                    <div class="row">
                        @foreach($permissionParent as $per)
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <label>
                                    <input class="checkbox_wrapper" type="checkbox">{{ $per->name }}
                                </label>
                            </div>
                            <div class="panel-body">
                                @foreach($per->permissionChildrent as $per_childrent)
                                <label class="col-md-4">
                                    <input type="checkbox" class="checkbox_childrent" name="permission_id[]" value="{{ $per_childrent->id }}">{{ $per_childrent->name }}
                                </label>
                                @endforeach
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <button type="submit" class="btn btn-default">Thêm mới</button>
                    <button type="reset" class="btn btn-default">Reset</button>
                </form>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
@endsection

@section('script')
@include('js.admin.roles.create')
@endsection