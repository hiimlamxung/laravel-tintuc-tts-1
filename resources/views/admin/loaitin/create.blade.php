@extends('admin.layout.index')

@section('content')
<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Loại tin
                            <small>Thêm mới:</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                        @if(count($errors) > 0)
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $value)
                            {{ $value }} <br>
                            @endforeach
                        </div>
                        @endif

                        @if(session('thongbao') !== null)
                        <div class="alert alert-success">
                            {{ session('thongbao') }}
                        </div>
                        @endif
                        <form action="{{ route('loaitin.store') }}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label>Thể loại:</label>
                                <select class="form-control" name="idTheLoai">
                                    @foreach($theloai as $value)
                                    <option value="{{ $value->id }}">{{ $value->Ten }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Tên loại tin:</label>
                                <input class="form-control" name="Ten" placeholder="Nhập tên loại tin" />
                            </div>

                            <button type="submit" class="btn btn-default">Thêm mới</button>
                            <button type="reset" class="btn btn-default">Reset</button>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection